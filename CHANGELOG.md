# Changelog

## v1.1.3

  * Fixed ArtifactHub image annotations

## v1.1.2

  * Added workaround for ArtifactHub Image Scanning Errors (see https://github.com/artifacthub/hub/issues/1387)

## v1.1.1

  * Fixed Chart icon URL (broken due to branch renaming)

## v1.1.0

  * Changed default ImagePullPolicy to `IfNotPresent`
  * Added support for custom labels and annotations for DaemonSet and StatefulSet Pod templates (#6)

## v1.0.7

  * Upgraded to hcloud-csi-driver v1.5.3

## v1.0.6

  * Fixed image tag for version 1.5.2 -> v1.5.2

## v1.0.5

  * Upgraded to hcloud-csi-driver v1.5.2

## v1.0.4

  * Upgraded to hcloud-csi-driver v1.5.1 (#3)
  * Introduced configuration variables `csiDriver.image` and `csiDriver.imagePullPolicy`

## v1.0.3

  * Upgraded to hcloud-csi-driver v1.5.0 (#2)

## v1.0.2

  * Fixed naming interference with hcloud-cloud-controller-manager helm chart (#1)

## v1.0.1

  * Added logo/icon

## v1.0.0

  * First release
